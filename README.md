Este projeto é parte de um modelo de avaliação e recrutamento de desenvolvedores para o Projeto `Shell Box`. Sendo assim, o software em questão não será utilizado pela mesma para fins lucrativos.

## Avaliação
A avaliação consiste na reprodução de uma **aplicação** e um **serviço web** `to do list` com a utilização obrigatória das tecnologias Angular 2+, NodeJs e MySQL.

#### Critérios de Avaliação

- Organização de código
- Documentação de código
- Boas práticas de desenvolvimento
- Técnicas de perfomance
- Realização das features obrigatórias
- Realização das features não obrigatórias

#### Features obrigatórias

- Adicionar uma nova task
- Listar tasks
  - Utilizar paginação com **10 itens por página**

#### Features não obrigatórias
Estas features podem ser implementadas e serão avaliadas como pontuação extra.

- Editar task
- Remover tasks
- Pesquisar por uma task

## Importante
- O sistema deverá ser composto por uma aplicação `Angular 2+` que consumirá um serviço `NodeJS + MySQL`.
- O banco de dados deverá seguir a normalização de dados na `2FN` (Segunda Forma Normal)
- O candidato poderá utilizar a internet para consultar documentação dos frameworks e ferramentas aqui citadas
- Não é permitido sobre qualquer hipótese consultar outras pessoas
- Não é permitido sobre qualquer hipótese consultar códigos prontos
- O candidato poderá adicionar novos elementos visuais e funcionais que possa contribuir para aumentar a sua pontuação
- É permitido altenar sobre framework CSS/HTML, por padrão o [UIKit](https://getuikit.com/v2/) foi utilizado para a construção da aplicação base
- Hora de início e término da prova prática será determinada pelos entrevistadores no momento da entrevista


## Ambiente (Somente para maquinas Raizen)

- Sistema Operacional: Windows 10 - 64 bits
- Node JS: 6.11.1
- Angular/Cli: 1.2.3
- MySQL: 8.0
- GitKraken: 2.7
- Visual Studio Code: 1.14.2
- Postman 5.0.2

## Entrega
Como resultado da prova prática, o candidato deverá enviar um email para `alberto.neto@raizen.com` com os respectivos links:

- Link do bitbucket do serviço NodeJs
- Link do bitbucket (fork) da aplicação Prova To Do

## Screenshot
![demo](https://bytebucket.org/acelera/prova-todo/raw/f420c83de0d8519548e717dd882d3a6287f0f3fa/demo.png)